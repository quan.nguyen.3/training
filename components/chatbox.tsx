import styles from '../styles/chatbox.module.css'
import { Button } from 'react-bootstrap'

const Chatbox: React.FC = () => {
    return (
        <div className={styles.chatbox}>
            <h5>Chatbox</h5>
            <ul id="chat-messages">
                {/* Chat messages will go here */}
            </ul>
            <div className={styles.inputRow}>
                <input className={styles.input} type="text" placeholder="Enter your message" />
                <Button className={styles.send} variant="secondary">Send</Button>
            </div>
        </div>
    )
}

export default Chatbox
