import styles from '../styles/invite.module.css'
import { Button } from 'react-bootstrap'

const Invite: React.FC = () => {
    return (
        
            <div className={styles.inputRow}>
                <input className={styles.input} type="text" placeholder="Enter your message" />
                <Button className={styles.invite} variant="secondary">Send</Button>
            </div>
    )
}

export default Invite