import { useRouter } from 'next/router'
import { Button } from 'react-bootstrap'
import styles from '../styles/admin.module.css'

const AdminPage: React.FC = () => {
  const router = useRouter()

  return (
    <div className={styles.inputContainer}>
      <div className={styles.inputRow}>
        <label className={styles.label} htmlFor="maxParticipants">Maximum participants:</label>
        <input className={styles.input} id="maxParticipants" type="number" defaultValue={10} min={2}/>
        <Button onClick={() => router.push('/room')} className={styles.button} variant="secondary">Create room</Button>
      </div>
    </div>
  )
}

export default AdminPage
