import { useRouter } from 'next/router'
import { Button } from 'react-bootstrap'
import styles from '../styles/client.module.css'

const ClientPage: React.FC = () => {
  const router = useRouter()

  return (
    <div className={styles.inputContainer}>
      <div className={styles.inputRow}>
        <input className={styles.input} type="text" placeholder="Enter your name" />
      </div>
      <div className={styles.inputRow}>
        <input className={styles.input} type="text" placeholder="Enter meeting ID" />
        <Button onClick={() => router.push('/room')} className={styles.button} variant="secondary">Join meeting</Button>
      </div>
    </div>
  )
}

export default ClientPage
