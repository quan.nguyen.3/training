import { useRouter } from 'next/router'
import AdminPage from '../components/admin'
import ClientPage from '../components/client'
import styles from '../styles/index.module.css'
import '../styles/globals.css'

const Home: React.FC = () => {
  const router = useRouter()
  const isAdmin = false;

  return (
    <div className={styles.body}>
      <div className={styles.container}>
        <h1 className={styles.title}>Online Meeting</h1>
        <p className={styles.subtitle}>Cuộc họp video chất lượng. Miễn phí cho tất cả người dùng.</p>
        {isAdmin ? <AdminPage /> : <ClientPage />}
      </div>
    </div>
  )
}

export default Home
