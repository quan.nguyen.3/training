import { useRouter } from 'next/router'
import styles from '../styles/room.module.css'
import '../styles/globals.css'
import Chatbox from '../components/chatbox'
import Invite from '../components/invite'

const Room: React.FC = () => {
    const router = useRouter()
    const isAdmin = false;
  
    return (
      <div className={styles.body}>
        <div className={styles.video}>
            <div className={styles.videoContainer}>
                <div className={styles.grid}>
                {Array(25).fill(0).map((_, i) => (
                    <div key={i} className={styles.participant}>
                    <video autoPlay muted>
                        {/* Participant's video stream */}
                    </video>
                    </div>
                ))}
                </div>
            </div>
            <div className={styles.controlsContainer}>
                <div className={styles.controls}>
                <button className={styles.button}>Microphone</button>
                <button className={styles.button}>Video</button>
                <button className={styles.button}>Leave</button>
                </div>
            </div>
        </div>
        <div className={styles.participants}>
            <Chatbox />
            <div className={styles.name}>
                <h5>Participants</h5>
                <ul id="memberNames">
                    {/* Chat messages will go here */}
                </ul>
                
            {isAdmin && <Invite />}
            </div>
        </div>
      </div>
    )
  }
  
  export default Room
